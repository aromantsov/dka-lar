<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Category;

class DashboardController extends Controller
{
    public function dashboard()
    {
    	return view('admin.dashboard', [
            'categories' => Category::latestCategories(5),
            'articles' => Article::where('published', 1)->orderBy('created_at')->get(),
            'count_categories' => Category::count(),
            'count_articles' => Article::count(),
    	]);
    }
}
