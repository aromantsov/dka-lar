<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function create()
    {
    	return view('admin.images.create');
    }

    public function upload(Request $request)
    {
    	$path = $request->file('image')->store('uploads', 'public');
    	return view('admin.images.create', ['path' => $path]);
    }
}
