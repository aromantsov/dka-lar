<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
      return view('admin.setting', Setting::getSettings());
    }

    public function store(Request $request)
    {
      Setting::where('key', '!=', NULL)->delete();

      foreach($request->except('_token') as $key => $value){
        $setting = new Setting;
        $setting->key = $key;
        $setting->value = $request->$key;
        $setting->save();
      }
      return redirect()->route('setting.index');
    }

}
