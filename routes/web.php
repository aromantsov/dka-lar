<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/blog/category/{slug?}', 'BlogController@category')->name('category');
Route::get('/blog/article/{slug?}', 'BlogController@article')->name('article');

Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
	Route::get('/', 'DashboardController@dashboard')->name('admin.index');
	Route::resource('/category', 'CategoryController', ['as'=>'admin']);
	Route::resource('/article', 'ArticleController', ['as'=>'admin']);
	Route::get('/image/create', 'ImageController@create')->name('image.create');
    Route::post('/image/upload', 'ImageController@upload')->name('image.upload');
	Route::group(['prefix'=>'user_management', 'namespace'=>'UserManagement'], function(){
		Route::resource('/user', 'UserController', ['as'=>'admin.user_management']);
	});
  Route::get('/tb', 'TelbotController@index')->name('admin.tb');
  Route::get('/setting', 'SettingController@index')->name('setting.index');
  Route::post('/setting/store', 'SettingController@store')->name('setting.store');
});

Route::get('/', function () {
    return view('blog.home');
});

Auth::routes();

Route::match(['post', 'get'], 'register', function(){
  Auth::logout();
  return redirect('/');
})->name('register');

Route::get('/home', 'HomeController@index')->name('home');
