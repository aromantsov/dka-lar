@extends('admin.layouts.app_admin')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Create of user @endslot
	@slot('parent') Dashboard @endslot
	@slot('active') Users @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.user_management.user.store') }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		@include('admin.user_management.users.partials.form')
	</form>
</div>
@endsection