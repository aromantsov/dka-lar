@extends('admin.layouts.app_admin')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') User List @endslot
	@slot('parent') Dashboard @endslot
	@slot('active') Userss @endslot
	@endcomponent
	<hr>
	<a href="{{ route('admin.user_management.user.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Create User</i></a>
	<table class="table table-striped">
		<thead>
			<th>Name</th>
			<th>Email</th>
			<th class="text-right">Action</th>
		</thead>
		<tbody>
			@forelse($users as $user)
            <tr>
            	<td>{{ $user->name }}</td>
            	<td>{{ $user->email }}</td> 
            	<td class="text-right">
            		<form action="{{ route('admin.user_management.user.destroy', $user) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.user_management.user.edit', $user) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
            		</form></td>
            </tr>
			@empty
            <tr>
            	<td colspan="3" class="text-center"><h2>No user</h2></td>
            </tr>
			@endforelse
			<tfoot>
				<tr>
					<td colspam="3">
						<ul class="pagination pull-right">
							{{ $users->links() }} 
						</ul>
					</td>
				</tr>
			</tfoot>
		</tbody>
	</table>
</div>
@endsection