@extends('admin.layouts.app_admin')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Edit of User @endslot
	@slot('parent') Dashboard @endslot
	@slot('active') Users @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.user_management.user.update', $user) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
    @include('admin.user_management.users.partials.form')
	</form>
</div>
@endsection