<label for="">Status</label>
<select name="published" id="" class="form-control">
@if(isset($article->id))
    <option value="0" @if($article->published == 0) selected @endif>Not published</option>
    <option value="1" @if($article->published == 1) selected @endif>Published</option>
@else
    <option value="0">Not published</option>
    <option value="1">Published</option>
@endif
</select>

<label for="">Title</label>
<input type="text" class="form-control" name="title" placeholder="title" value="{{ $article->title or '' }}" required>

<label for="">Slug</label>
<input type="text" class="form-control" name="slug" placeholder="Automatic Generation" value="{{ $article->slug or '' }}" readonly="">  

<label for="">Category</label>
<select name="categories[]" id="" class="form-control" multiple="">
	<option value="0">--Without Category--</option>
	@include('admin.articles.partials.categories', ['categories' => $categories])
</select>

<label for="description-short">Short Description</label>
<textarea name="description_short" id="description-short" cols="30" rows="10" class="form-control"></textarea>


<label for="description">Description</label>
<textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
<script type="text/javascript">
$(document).ready(function(){
    CKEDITOR.replace( 'description-short' );
    CKEDITOR.replace( 'description' );
});
 </script>

<label for="meta_title">Meta Title</label>
<input type="text" name="meta_title" id="meta_title" cols="30" rows="10" class="form-control">

<label for="meta_description">Meta description</label>
<input type="text" name="meta_description" id="meta_description" cols="30" rows="10" class="form-control">

<label for="keywords">Keywords</label>
<input type="text" name="meta_keyword" id="keywords" cols="30" rows="10" class="form-control">
<hr>
<input type="submit" name="some_name" class="btn btn-primary" value="save">

  