@extends('admin.layouts.app_admin')

@section('content')
<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') Edit of Article @endslot
	@slot('parent') Dashboard @endslot
	@slot('active') Articles @endslot
	@endcomponent
	<hr>
	<form action="{{ route('admin.article.update', $article) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		<label for="">Status</label>
<select name="published" id="" class="form-control"> 
@if(isset($article['id']))
    <option value="0" @if($article['published'] == 0) selected @endif>Not published</option>
    <option value="1" @if($article['published'] == 1) selected @endif>Published</option>
@else
    <option value="0">Not published</option>
    <option value="1">Published</option>
@endif
</select>

<label for="">Title</label>
<input type="text" class="form-control" name="title" placeholder="title" value="{{ $article['title'] or '' }}" required>

<label for="">Slug</label>
<input type="text" class="form-control" name="slug" placeholder="Automatic Generation" value="{{ $article['slug'] or '' }}" readonly="">  

<label for="">Category</label>
<select name="categories[]" id="" class="form-control" multiple="">
	<option value="0">--Without Category--</option>
	@foreach ($categories as $category)

  <option value="{{$category->id or ""}}"

    @isset($article['id'])
      @foreach ($article['categories'] as $category_article)
        @if ($category->id == $category_article)
          selected="selected"
        @endif
      @endforeach
    @endisset

    >
    {!! $delimiter or "" !!}{{$category->title or ""}}
  </option>

  @if (count($category->children) > 0)

    @include('admin.articles.partials.categories', [
      'categories' => $category->children,
      'delimiter'  => ' - ' . $delimiter
    ])

  @endif
@endforeach
</select>

<label for="description-short">Short Description</label>
<textarea name="description_short" id="description-short" cols="30" rows="10" class="form-control">{{ $article['description_short'] or '' }}</textarea>


<label for="description">Description</label>
<textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $article['description'] or '' }}</textarea>
<script type="text/javascript">
$(document).ready(function(){
    CKEDITOR.replace( 'description-short' );
    CKEDITOR.replace( 'description' );
});
 </script>

<label for="meta_title">Meta Title</label>
<input type="text" name="meta_title" id="meta_title" cols="30" rows="10" class="form-control" value="{{ $article['meta_title'] or '' }}">

<label for="meta_description">Meta description</label>
<input type="text" name="meta_description" id="meta_description" cols="30" rows="10" class="form-control" value="{{ $article['meta_description'] or '' }}">

<label for="keywords">Keywords</label>
<input type="text" name="meta_keyword" id="keywords" cols="30" rows="10" class="form-control" value="{{ $article['keyword'] or '' }}">
<hr>
<input type="submit" name="some_name" class="btn btn-primary" value="save">

  
		<input type="hidden" name="modified_by" value="{{Auth::id()}}">
	</form>
</div>
@endsection