@extends('admin.layouts.app_admin')

@section('content')

<div class="container">
	@component('admin.components.breadcrumb')
	@slot('title') List of articles @endslot
	@slot('parent') Main page @endslot
	@slot('active') Articles @endslot
	@endcomponent
	<hr/>
	<a href="{{ route('admin.article.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"> Create article</i></a>
	<table class="table table-striped">
		<thead>
			<th>Name</th>
			<th>Publish</th>
			<th class="text-right">Action</th>
		</thead>
		<tbody>
			@forelse($articles as $article)
			<tr>
				<td>{{ $article->title }}</td>
				<td>{{ $article->published }}</td>
				<td class="text-right">
				    <form action="{{ route('admin.article.destroy', $article) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
            			<input type="hidden" name="_method" value="DELETE"> 
            			{{ csrf_field() }}
            			<a href="{{ route('admin.article.edit', $article) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
            			<button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
            		</form></td>
			</tr>
			@empty
			<tr>
				<td colspan="3" class="text-center">No articles</td>
			</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination full-right">
						{{ $articles->links() }}
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection