<label for="">Status</label>
<select name="published" id="" class="form-control">
@if(isset($category->id))
<option value="0" @if($category->published == 0) selected="" @endif>Not published</option>
<option value="1" @if($category->published == 1) selected="" @endif>Published</option>
@else
<option value="0">Not published</option>
<option value="1">Published</option>
@endif
</select>
<label for="">Name</label>
<input type="text" class="form-control" name="title" placeholder="name" value="{{ $category->title or '' }}" required>
<label for="">Slug</label>
<input type="text" class="form-control" name="slug" placeholder="generated automatically" value="{{ $category->slug or '' }}" readonly="">
<label for="">Parent category</label>
<select name="parent_id" id="" class="form-control">
	<option value="0">--Without parent category--</option>
	@include('admin.categories.partials.categories', ['categories'=>$categories])
</select>
<hr/>
<input type="submit" name="some_name" class="btn btn-primary" value="save">    