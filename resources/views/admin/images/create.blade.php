@extends('admin.layouts.app_admin')

@section('content')
<div class="container">
	<h1>Image title var 1</h1>
	<form action="{{ route('image.upload') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group">
			<input type="file" name="image">
		</div>
		<button class="btn btn-default">Upload</button>
	</form>
	@isset($path)
	<img src="{{ asset('/storage/' . $path) }}" alt="" class="img-fluid">
	@endisset
</div>
@endsection