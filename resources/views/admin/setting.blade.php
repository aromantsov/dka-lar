@extends('admin.layouts.app_admin')

@section('content')
<div class="content">
  <form action="{{ route('setting.store') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label>Url calback for Telegram Bot</label>
      <div class="input-group-btn">
        <button type="botton" class="btn btn=default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" onclick="document.getElementById('url_callback_bot').value = '{{ url('') }}'">Insert url</a></li>
          <li><a href="#">Send url</a></li>
          <li><a href="#">Get info</a></li>
        </ul>
      </div>
      <input type="url" name="url_callback_bot" class="form_control" id="url_callback_bot" value="{{ $url_callback_bot or '' }}">
    </div>
    <div class="form-group">
      <label>Site name for Main Page</label>
      <input type="text" name="site_name" class="form_control" id="site_name" value="{{ $site_name or '' }}">
    </div>
    <button class="btn btn-primary" type="submit">Send</button>
  </form>
</div>
@endsection